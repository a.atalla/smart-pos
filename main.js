require('electron-reload')(__dirname)
// const {
//   default: installExtension, REDUX_DEVTOOLS   // REACT_DEVELOPER_TOOLS
// } = require('electron-devtools-installer')
//
// installExtension(REDUX_DEVTOOLS)
//   .then((name) => console.log(`Added Extension:  ${name}`))
//   .catch((err) => console.log('An error occurred: ', err))

const electron = require('electron')
const {app} = electron
const BrowserWindow = electron.BrowserWindow

let mainWindow

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    height: 800,
    width: 1200
  })
  // mainWindow.maximize();
  mainWindow.loadURL(`file:///${__dirname}/index.html`)
  mainWindow.webContents.openDevTools()

  mainWindow.on('close', () => {
    mainWindow = null
  })
})
