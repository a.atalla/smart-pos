const webpack = require('webpack')

module.exports = {

  entry: [
    './app/App.js'
  ],

  output: {
    path: __dirname,
    sourceMapFilename: 'public/js/bundle.js.map',
    filename: 'public/js/bundle.js'
  },

  devtool: 'inline-source-map',
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'stage-0', 'react']
      }
    }, {
      test: /\.css$/, loader: 'style-loader!css-loader!autoprefixer-loader'
    }, {
      test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'url-loader?limit=1024&mimetype=application/font-woff&name=public/fonts/[name].[ext]'
    }, {
      test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'file-loader?limit=1024&name=public/fonts/[name].[ext]'
    }]
  }
}
