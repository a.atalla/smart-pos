
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('products').del()
    .then(function () {
      // Inserts seed entries
      return knex('products').insert([
        {name: 'product 1'},
        {name: 'product 2'},
        {name: 'product 3'},
        {name: 'product 4'},
      ]);
    });
};
