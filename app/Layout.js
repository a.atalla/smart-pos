import React from 'react';
import Sidebar from './Sidebar';


const Layout = (props)=>{
  return(
    <div id="wrapper">
    <Sidebar/>
    <nav id="top-nav" className="nav">
      <div className="nav-left">
      <a className="nav-item">
        <input type="text" className="input" placeholder="Search"/>
      </a>
      </div>
    </nav>
    <div id="main-content">
      {props.children}
    </div>
  </div>
  )
}

export default Layout;