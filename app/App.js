import React from 'react'
import {Router, Route, IndexRoute, hashHistory} from 'react-router'
import ReactDOM from 'react-dom'
import {combineReducers, createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'

import './static/css/bootstrap.min.css'
import './static/css/bootstrap-rtl.min.css'
import 'font-awesome/css/font-awesome.css'
import './static/css/main.css'

import {productsReducer} from './reducers/productReducers'
import Layout from './Layout'
import HomePage from './pages/HomePage'
import ProductsPage from './pages/ProductsPage'

import {fetchAllProducts, startCountProducts} from './actions/productActions'

const reducer = combineReducers({
  products: productsReducer
})

const store = createStore(reducer, applyMiddleware(thunk))
store.dispatch(fetchAllProducts())
store.dispatch(startCountProducts())
ReactDOM.render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path='/' component={Layout} >
        <IndexRoute component={HomePage} />
        <Route path='products' component={ProductsPage} />
      </Route>
    </Router>
  </Provider>, document.getElementById('target')
)
