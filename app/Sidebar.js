import React from 'react';
import {Link, IndexLink} from 'react-router';


const Sidebar = ()=>{
  return (
    <div id="side-nav" className="">
      <ul>
        <li title="Home">
          <IndexLink to="/" activeClassName="active">
            <i className="fa fa-home"> </i>
          </IndexLink>
        </li>
        <li title="Products">
          <Link to="products" activeClassName="active">
            <i className="fa fa-archive"> </i>
          </Link>
        </li>
        <li title="Sales">
          <Link to="sales" activeClassName="active">
            <i className="fa fa-money"> </i>
          </Link>
        </li>
        <li title="Vendors">
          <Link to="vendors" activeClassName="active">
            <i className="fa fa-user-circle"> </i>
          </Link>
        </li>
        <li title="Customers">
          <Link to="customers" activeClassName="active">
            <i className="fa fa-user-circle-o"> </i>
          </Link>
        </li>
      </ul>
    </div>
  )
}

export default Sidebar;
