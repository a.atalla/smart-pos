import React, { Component } from 'react'
import {connect} from 'react-redux'
import ProductForm from './products/ProductForm'
import { Button, Table } from 'react-bootstrap'

class ProductsPage extends Component {

  constructor (props) {
    super(props)
    this.state = {
      isFormOpen: false,
      currentPage: 1
    }
  }

  openProductForm () {
    this.setState({
      isFormOpen: true
    })
  }

  closeProductForm () {
    this.setState({
      isFormOpen: false
    })
  }

  renderProductsList () {
    return this.props.allProducts.map((prod) => {
      return (
        <tr key={prod.id}>
          <td>{prod.id}</td>
          <td>{prod.name}</td>
          <td>13.234</td>
          <td>13.234</td><td>13.234</td><td>13.234</td>
        </tr>
      )
    })
  }

  getPrevPage () {
    // dec currentPage
    if (this.state.currentPage > 1) {
      this.setState({
        currentPage: this.state.currentPage - 1
      })
    }
  }

  getNextPage () {
    // inc currentPage
    this.setState({
      currentPage: this.state.currentPage + 1
    })
  }

  render () {
    console.log('All Products: ', this.props.allProducts)
    const paginationStyle = {
      border: '1px solid #e5e5e5',
      borderRadius: '5px',
      margin: '5px'
    }

    return (
      <div>
        <div style={{marginBottom: '10px'}}>
          <h1 style={{fontSize: '2em'}}>قائمة المنتجات</h1>
          <Button bsStyle='primary'
            onClick={this.openProductForm.bind(this)}>
            إضافة منتج</Button>
        </div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>مسلسل</th>
              <th>إسم المنتج</th>
              <th>السعر</th>
              <th>السعر</th>
              <th>السعر</th>
              <th>السعر</th>

            </tr>
          </thead>
          <tbody>
            {this.renderProductsList()}
          </tbody>
        </Table>
        <nav className='pagination is-centered is-small'>
          <a className='pagination-previous'
            style={paginationStyle}
            onClick={this.getPrevPage.bind(this)}>السابق</a>
          <a className='pagination-next'
            style={paginationStyle}
            onClick={this.getNextPage.bind(this)}>التالي</a>
        </nav>
        <ProductForm isActive={this.state.isFormOpen} close={this.closeProductForm.bind(this)} />
      </div>
    )
  }
}

export default connect((state) => {
  return {
    allProducts: state.products
  }
})(ProductsPage)
