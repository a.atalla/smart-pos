import React, { Component } from 'react'
import {connect} from 'react-redux'
import { startAddProduct } from '../../actions/productActions'
import { Button, Modal, FormGroup, ControlLabel, FormControl } from 'react-bootstrap'

class ProductForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      isActive: false
    }
  }

  componentWillReceiveProps (newProps) {
    this.setState({
      isActive: newProps.isActive
    })
  }

  handleFormSubmit (e) {
    e.preventDefault()
    this.props.dispatch(startAddProduct({
      name: e.target.name.value
    }))
    this.props.close()
  }

  render () {
    return (
      <Modal show={this.state.isActive}>
        <Modal.Header>
          <Modal.Title>إضافة منتج جديد</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <form onSubmit={this.handleFormSubmit.bind(this)}>
            <FormGroup>
              <ControlLabel>إسم المنتج</ControlLabel>
              <FormControl
                type='text'
                name='name'
              />
            </FormGroup>
          </form>
        </Modal.Body>

        <Modal.Footer>
          <Button bsStyle='danger' onClick={this.props.close}>Close</Button>
          <Button bsStyle='primary' type='submit'>Save changes</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

// export default ProductForm
// Connect with redux store, this will make dispatch via 'this.props.dispatch'
export default connect((state) => {
  return {

  }
})(ProductForm)
