export const addProducts = (products) => {
  return {
    type: 'ADD_PRODUCTS',
    products
  }
}

export const startAddProduct = (product) => {
  return (dispatch) => {
    knex('products')
      .returning(['id', 'name'])
      .insert(product)
      .then((result) => {
        console.log('saved: ', result)
        dispatch(addProducts(result))
      })
  }
}

export const countProducts = (totalProducts) => {
  return {
    type: 'COUNT_PRODUCTS',
    totalProducts
  }
}

export const startCountProducts = () => {
  return (dispatch) => {
    knex('products')
      .count()
      .then((total) => {
        console.log('Producst COunt: ', total[0].count)
      })
  }
}

export const fetchAllProducts = () => {
  return (dispatch) => {
    knex.select('*')
      .from('products')
      .limit(15)
      .then((products) => {
        dispatch(addProducts(products))
      })
  }
}
